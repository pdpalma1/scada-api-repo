create table alerta
(
  id                int auto_increment
    primary key,
  idElemento        int          not null,
  descripcion       varchar(200) not null,
  esResuelto        tinyint(1)   not null,
  alertaElemento    varchar(200) not null,
  nombreSensor      varchar(100) null,
  valorActualSensor int          null,
  valorMaximoSensor int          null
);

create table bomba
(
  id                   int          not null
    primary key,
  nombre               varchar(200) not null,
  tempCojineteSuperior float        not null,
  tempCojineteInferior float        not null,
  tempBobinaMotor      float        not null,
  presDescargaBomba    float        not null
);

create table compresor
(
  id          int   not null
    primary key,
  temperatura float null,
  presion     float null
);

create table histograma
(
  hora        int default 0 not null
    primary key,
  temperatura float         null
);

create table ordenes
(
  id             int(1) auto_increment
    primary key,
  descripcion    varchar(200)                        null,
  esResuelto     tinyint(1)                          null,
  numeroOrden    int                                 null,
  fechaHora      timestamp default CURRENT_TIMESTAMP not null,
  idElemento     varchar(100)                        null,
  prioridad      varchar(50)                         null,
  nombreElemento varchar(100)                        null
);

create table sensores
(
  id     int default 0 not null
    primary key,
  nombre varchar(200)  null,
  valor  int           null
);

create table variador
(
  activo tinyint(1) null,
  id     int auto_increment
    primary key
);