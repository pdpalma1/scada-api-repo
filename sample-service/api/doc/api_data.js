define({ "api": [
  {
    "type": "get",
    "url": "/alertas",
    "title": "Obtiene todas las alertas",
    "group": "Alertas",
    "version": "0.0.1",
    "filename": "./src/routes/endpoints/alertas.js",
    "groupTitle": "Alertas",
    "name": "GetAlertas"
  },
  {
    "type": "get",
    "url": "/alertas/:id",
    "title": "Trae alerta por id",
    "group": "Alertas",
    "version": "0.0.1",
    "filename": "./src/routes/endpoints/alertas.js",
    "groupTitle": "Alertas",
    "name": "GetAlertasId"
  },
  {
    "type": "put",
    "url": "/alertas/actualizacion/:id",
    "title": "Actualiza columna esResuelto",
    "group": "Alertas",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "Valor de la columna esResuelto",
          "content": "{\n  \"esResuelto\": bool\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/alertas.js",
    "groupTitle": "Alertas",
    "name": "PutAlertasActualizacionId"
  },
  {
    "type": "put",
    "url": "/alertas/:id",
    "title": "Actualiza alerta por id",
    "group": "Alertas",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "Algunos valores de bomba",
          "content": "{\n  \"descripcion\": string,\n  \"esResuelto\": bool\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/alertas.js",
    "groupTitle": "Alertas",
    "name": "PutAlertasId"
  },
  {
    "type": "get",
    "url": "/bomba",
    "title": "Obtener todas las bombas",
    "group": "Bombas",
    "version": "0.0.1",
    "filename": "./src/routes/endpoints/bomba.js",
    "groupTitle": "Bombas",
    "name": "GetBomba"
  },
  {
    "type": "get",
    "url": "/bomba/:id",
    "title": "Obtener bombas por id",
    "group": "Bombas",
    "version": "0.0.1",
    "filename": "./src/routes/endpoints/bomba.js",
    "groupTitle": "Bombas",
    "name": "GetBombaId"
  },
  {
    "type": "post",
    "url": "/bomba/insert",
    "title": "Postea una bomba",
    "group": "Bombas",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "Parametros para cargar una bomba",
          "content": " {\n\"id\": req.body.id,\n  \"nombre\": string,\n  \"tempCojineteSuperior\" : float,\n   \"tempCojineteInferior\" : float,\n   \"tempBobinaMotor\" : float,\n   \"presDescargaBomba\" : float\n }",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/bomba.js",
    "groupTitle": "Bombas",
    "name": "PostBombaInsert"
  },
  {
    "type": "put",
    "url": "/bomba/presDescargaBomba/:id",
    "title": "Actualiza valor de presDescargaBomba",
    "group": "Bombas",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "Valor del sensor  presDescargaBomba",
          "content": "{\n  \" presDescargaBomba\": float\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/bomba.js",
    "groupTitle": "Bombas",
    "name": "PutBombaPresdescargabombaId"
  },
  {
    "type": "put",
    "url": "/bomba/tempBobinaMotor/:id",
    "title": "Actualiza valor de tempBobinaMotor",
    "group": "Bombas",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "Valor del sensor  tempBobinaMotor",
          "content": "{\n  \" tempBobinaMotor\": float\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/bomba.js",
    "groupTitle": "Bombas",
    "name": "PutBombaTempbobinamotorId"
  },
  {
    "type": "put",
    "url": "/bomba/tempCojineteInferior/:id",
    "title": "Actualiza valor de tempCojineteInferior",
    "group": "Bombas",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "Valor del sensor  tempCojineteInferior",
          "content": "{\n  \" tempCojineteInferior\": float\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/bomba.js",
    "groupTitle": "Bombas",
    "name": "PutBombaTempcojineteinferiorId"
  },
  {
    "type": "put",
    "url": "/bomba/tempCojineteSuperior/:id",
    "title": "Actualiza valor de tempCojineteSuperior",
    "group": "Bombas",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "Valor del sensor  tempCojineteSuperior",
          "content": "{\n  \" tempCojineteSuperior\": float\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/bomba.js",
    "groupTitle": "Bombas",
    "name": "PutBombaTempcojinetesuperiorId"
  },
  {
    "type": "get",
    "url": "/",
    "title": "Endpoint Vacio",
    "name": "_",
    "group": "Errores",
    "version": "0.0.0",
    "filename": "./src/routes/endpoints/errores.js",
    "groupTitle": "Errores"
  },
  {
    "type": "get",
    "url": "/ordenes",
    "title": "Obtener todas las ordenes",
    "group": "Ordenes",
    "version": "0.0.1",
    "filename": "./src/routes/endpoints/ordenes.js",
    "groupTitle": "Ordenes",
    "name": "GetOrdenes"
  },
  {
    "type": "get",
    "url": "/ordenes/:id",
    "title": "Obtener todas las ordenes por id",
    "group": "Ordenes",
    "version": "0.0.1",
    "filename": "./src/routes/endpoints/ordenes.js",
    "groupTitle": "Ordenes",
    "name": "GetOrdenesId"
  },
  {
    "type": "post",
    "url": "/ordenes/insert",
    "title": "Postea todos los parametros de una orden",
    "group": "Ordenes",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "Parametros de una orden",
          "content": "    {\n      \"descripcion\": string,\n\"esResuelto\" : bool,\n\"numeroOrden\" : rows.length + 1,\n\"idElemento\" : int,\n\"prioridad\" : string,\n\"nombreElemento\" : string\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/ordenes.js",
    "groupTitle": "Ordenes",
    "name": "PostOrdenesInsert"
  },
  {
    "type": "put",
    "url": "/ordenes/actualizacion/:id",
    "title": "Actualiza columna esResuelto por id",
    "group": "Ordenes",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "columna esResuelto",
          "content": "{\n  \" esResuelto\": bool\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/ordenes.js",
    "groupTitle": "Ordenes",
    "name": "PutOrdenesActualizacionId"
  },
  {
    "type": "put",
    "url": "/ordenes/actualizacion/todo/:id",
    "title": "Actualiza columnas descripcion y esResuelto por id",
    "group": "Ordenes",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "descripcion y esResuleto",
          "content": "    {\n      \" descripcion\": string,\n\"esResuelto\" : bool\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/ordenes.js",
    "groupTitle": "Ordenes",
    "name": "PutOrdenesActualizacionTodoId"
  },
  {
    "type": "put",
    "url": "/ordenes/control/:id",
    "title": "Actualiza las ordenes desde el control de la app android",
    "group": "Ordenes",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "Todos los valores desde el control",
          "content": "    {\n      \" descripcion\": string,\n\"esResuelto\":bool,\n\"prioridad\":string,\n\"nombreElemento\":string,\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/ordenes.js",
    "groupTitle": "Ordenes",
    "name": "PutOrdenesControlId"
  },
  {
    "type": "put",
    "url": "/sensores/:id",
    "title": "Actualiza columna valor de sensores",
    "group": "Sensores",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "Valor del sensor",
          "content": "{\n  \"valor\": float\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/alertas.js",
    "groupTitle": "Sensores",
    "name": "PutSensoresId"
  },
  {
    "type": "get",
    "url": "/variador",
    "title": "Obtener si el variador esta activo o no",
    "group": "Simulador",
    "version": "0.0.1",
    "filename": "./src/routes/endpoints/simulador.js",
    "groupTitle": "Simulador",
    "name": "GetVariador"
  },
  {
    "type": "put",
    "url": "/variador/:id",
    "title": "Actualiza valor del variador",
    "group": "Simulador",
    "version": "0.0.1",
    "parameter": {
      "examples": [
        {
          "title": "activo o no activo",
          "content": "{\n  \" activo\": bool\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/routes/endpoints/simulador.js",
    "groupTitle": "Simulador",
    "name": "PutVariadorId"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "_root_Documentos_proyectos_pampa_energia_api_master_doc_main_js",
    "groupTitle": "_root_Documentos_proyectos_pampa_energia_api_master_doc_main_js",
    "name": ""
  }
] });
