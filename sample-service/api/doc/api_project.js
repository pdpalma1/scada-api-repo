define({
  "name": "Api Pampa Energia AR",
  "version": "0.0.1",
  "description": "Api para usar datos relacionados con scada y sensores de distintas maquinarias",
  "title": "Api Pampa Energia AR",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-01-10T14:26:10.131Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
