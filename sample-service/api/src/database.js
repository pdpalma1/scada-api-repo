const mysql = require('mysql');

//prod database
/*const mysqlConnection = mysql.createConnection({
	host: 'us-cdbr-iron-east-01.cleardb.net',
	user: 'b47120a1fedad5',
	password: '9eb33eac',
	database: 'heroku_1b35dc1778e4abd',
	multipleStatemenets : true
});*/

//Dev database
/*const mysqlConnection = mysql.createConnection({
    host: 'us-cdbr-iron-east-01.cleardb.net',
    user: 'b2ad107128bb4b',
    password: 'f8442195',
    database: 'heroku_db21930e86614ce',
    multipleStatemenets : true
});*/


/*mysqlConnection.connect(function (err){
    if(err){
        console.error('No se pudo conectar la base de datos :( ' + err);
        return;
    }else{
        console.warn('Base de datos conectada');
    }
});

setInterval(function () {
    mysqlConnection.query('SELECT 1');
}, 10000);


module.exports = mysqlConnection;*/

//#region Local DB

//Local DB
var mysql = require('mysql');
var config = require('./config.js');

function startConnection() {
    console.error('CONNECTING');
    connection = mysql.createConnection(config.mysql);
    connection.connect(function(err) {
        if (err) {
            console.error('CONNECT FAILED', err.code);
            startConnection();
        }
        else
            console.error('CONNECTED');
    });
    connection.on('error', function(err) {
        if (err.fatal)
            startConnection();
    });
}

startConnection();


// testing a select every 3 seconds :
// to try the code you can stop mysql service => select will fail
// if you start mysql service => connection will restart correctly => select will succeed
setInterval(function() {
    connection.query('SELECT 1', function(err, results) {
        if (err) console.log('SELECT', err.code);
        else console.log('SELECT', results);
    });
}, 3000);