const express = require('express');
const app = express();
var figlet = require('figlet');



//Settings
app.set('port',process.env.PORT || 5000);



//Middleware
app.use(express.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");

    // Website you wish to allow to connect
    res.header('Access-Control-Allow-Origin', 'http://localhost:8100');

    // Request methods you wish to allow
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Set custom headers for CORS
    res.header("Access-Control-Allow-Headers", "Content-type,Accept,X-Custom-Header,XMLHttpRequest");

    if (req.method === "OPTIONS") {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
    } else {
        res.header('Access-Control-Allow-Origin', '*');
    }

    next();
});




//Routes
app.use(require('./routes/endpoints/bomba'));
app.use(require('./routes/endpoints/alertas'));
app.use(require('./routes/endpoints/errores'));
app.use(require('./routes/endpoints/ordenes'));
app.use(require('./routes/endpoints/simulador'));
app.use(require('./routes/endpoints/histograma'));

//Starting the server
app.listen(app.get('port'), ()=> {
    console.info("Servidor En puerto: " + app.get('port'));
    console.log(figlet.textSync('Pampa Energia Ar Api', {
    font: 'Standard',
    horizontalLayout: 'default',
    verticalLayout: 'default'
}));
});
