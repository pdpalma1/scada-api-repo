const express = require('express');
const router = express.Router();
const mysqlConnection = require('../../database');

var bombaID = true;
var estado = false;



function agregarAlerta(id,descripcion,estado,sensor,valorActual,maximo){
    let put = { descripcion : descripcion,esResuelto : estado, nombreSensor : sensor , valorActualSensor : valorActual, valorMaximoSensor : maximo};
    let sql = 'UPDATE alerta SET ? WHERE id =' + [id] ;
    let query = mysqlConnection.query(sql, put, (err, result) => {
        if(err){
            console.log(err);
    }

    });
}

function analizarTodo (num) {

    mysqlConnection.query('SELECT * FROM bomba ', (err, rows, fields) => {
        if (err) {
            console.log(err);
        } else {

            mysqlConnection.query('SELECT * FROM sensores ' , (err,valores,fields) => {
                if(err){
                    console.log(err);
                }else{



                    if (num == 0) {

                        if(rows[num].presDescargaBomba < valores[1].valor && rows[num].tempCojineteSuperior < valores[0].valor && rows[num].tempCojineteInferior < valores[0].valor && rows[num].tempBobinaMotor < valores[0].valor){
                            agregarAlerta(1, "", 1, "", 0,0);
                        }else{
                            if (rows[num].presDescargaBomba > valores[1].valor) {
                                agregarAlerta(1, "La presión de descarga de bomba es muy alta", 0, "Sensor de presion", rows[num].presDescargaBomba,valores[1].valor);
                            }
                            if (rows[num].tempCojineteSuperior > valores[2].valor) {
                                agregarAlerta(1, "La temperatura de cojinete superior es muy alta", 0, "Sensor de temperatura", rows[num].tempCojineteSuperior,valores[2].valor);
                            }
                            if (rows[num].tempCojineteInferior > valores[2].valor) {
                                agregarAlerta(1, "La temperatura de cojinete inferior es muy alta", 0, "Sensor de temperatura", rows[num].tempCojineteInferior,valores[2].valor);
                            }
                            if (rows[num].tempBobinaMotor > valores[3].valor) {
                                agregarAlerta(1, "La temperatura de bobina de motor", 0, "Sensor de temperatura", rows[num].tempBobinaMotor,valores[3].valor);
                            }
                        }


                    } else {

                        if(rows[num].presDescargaBomba < valores[1].valor && rows[num].tempCojineteSuperior < valores[0].valor && rows[num].tempCojineteInferior < valores[0].valor && rows[num].tempBobinaMotor < valores[0].valor){
                            agregarAlerta(2, "", 1, "", 0,0);
                        }else{
                            if (rows[num].presDescargaBomba > valores[1].valor) {
                                agregarAlerta(2, "La presión de descarga de bomba es muy alta", 0, "Sensor de presion", rows[num].presDescargaBomba,valores[1].valor);
                            }
                            if (rows[num].tempCojineteSuperior > valores[2].valor) {
                                agregarAlerta(2, "La temperatura de cojinete superior es muy alta", 0, "Sensor de temperatura", rows[num].tempCojineteSuperior,valores[2].valor);
                            }
                            if (rows[num].tempCojineteInferior > valores[2].valor) {
                                agregarAlerta(2, "La temperatura de cojinete inferior es muy alta", 0, "Sensor de temperatura", rows[num].tempCojineteInferior,valores[2].valor);
                            }
                            if (rows[num].tempBobinaMotor > valores[3].valor) {
                                agregarAlerta(2, "La temperatura de bobina de motor", 0, "Sensor de temperatura", rows[num].tempBobinaMotor,valores[3].valor);
                            }
                        }



                    }


                }
            });






        }
    });
}

setInterval(function () {

    if(bombaID == true){
        analizarTodo(0);
        bombaID = false;
    }else{
        analizarTodo(1);
        bombaID = true;
    }

},1000);





/**
 * @api {get} /alertas Obtiene todas las alertas
 * @apiGroup Alertas
 * @apiVersion 0.0.1
 */
router.get('/alertas',(req,res) => {
    mysqlConnection.query('SELECT * FROM alerta   ' , (err,rows,fields) => {
        if(err){
            console.log(err);
        }else{
            console.log("Alertas obtenidas");
            res.send(rows);
        }
    })
});




/**
 * @api {get} /alertas/:id Trae alerta por id
 * @apiGroup Alertas
 * @apiVersion 0.0.1
 */
router.get('/alertas/:id',(req,res) => {

    const { id } = req.params;
    mysqlConnection.query('SELECT * FROM alerta WHERE id = ? ', [id] , (err,rows,fields) => {
        if(err){
            console.log(err);
        }else{
            console.log("Alerta obtenida");
            res.send(rows[0]);
        }
    })
});




/**
 * @api {put} /alertas/actualizacion/:id Actualiza columna esResuelto
 * @apiGroup Alertas
 * @apiVersion 0.0.1
 * @apiParamExample {json} Valor de la columna esResuelto
 *     {
 *       "esResuelto": bool
 *     }
 */
router.put('/alertas/actualizacion/:id', (req, res) => {

    const { id } = req.params;
    let post = { esResuelto : req.body.esResuelto};
    let sql = 'UPDATE alerta SET ? WHERE id =' + [id] ;
    let query = mysqlConnection.query(sql, post, (err, result) => {
        if(err){
            console.log(err);
        }else{
            console.log("Alerta actualizada");
            console.log(req.body.esResuelto);
            res.send(result);
        }

    });
});



/**
 * @api {put} /sensores/:id Actualiza columna valor de sensores
 * @apiGroup Sensores
 * @apiVersion 0.0.1
 * @apiParamExample {json} Valor del sensor
 *     {
*       "valor": float
*     }
 */
router.put('/sensores/:id', (req, res) => {

    const { id } = req.params;
    let post = { valor : req.body.valor};
    let sql = 'UPDATE sensores SET ? WHERE id =' + [id] ;
    let query = mysqlConnection.query(sql, post, (err, result) => {
        if(err){
            console.log(err);
        }else{
            console.log("sensor actualizada");

            res.send(result);
        }

    });
});



/**
 * @api {put} /alertas/:id Actualiza alerta por id
 * @apiGroup Alertas
 * @apiVersion 0.0.1
 *@apiParamExample {json} Algunos valores de bomba
*     {
*       "descripcion": string,
*       "esResuelto": bool
*     }
 */
router.put('/alertas/actualizacion/todo/:id', (req, res) => {

    const { id } = req.params;
    let post = { descripcion : req.body.descripcion,esResuelto : req.body.esResuelto};
    let sql = 'UPDATE alerta SET ? WHERE id =' + [id] ;
    let query = mysqlConnection.query(sql, post, (err, result) => {
        if(err){
            console.log(err);
        }else{
            console.log("Alerta actualizada");
            console.log(req.body.esResuelto);
            res.send(result);
        }

    });
});



module.exports = router;
