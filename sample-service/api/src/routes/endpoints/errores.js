const express = require('express');
const router = express.Router();
const mysqlConnection = require('../../database');
var figlet = require('figlet');



/**
 * @api {get} / Endpoint Vacio
 * @apiName /
 * @apiGroup Errores
 *
 */
router.get('/',(req,res) => {
    res.json("Pampa Energia AR Api Server");

});



module.exports = router;
