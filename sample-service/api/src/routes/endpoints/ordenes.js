const express = require('express');
const router = express.Router();
const mysqlConnection = require('../../database');



/**
 * @api {get} /ordenes Obtener todas las ordenes
 * @apiGroup Ordenes
 * @apiVersion 0.0.1
 */
router.get('/ordenes',(req,res) => {
    mysqlConnection.query('SELECT * FROM ordenes   ' , (err,rows,fields) => {
        if(err){
            console.log(err);
        }else{
            console.log("Ordenes obtenidas");
            res.send(rows);
        }
    })
});


/**
 * @api {get} /ordenes/:id Obtener todas las ordenes por id
 * @apiGroup Ordenes
 * @apiVersion 0.0.1
 */
router.get('/ordenes/:id',(req,res) => {
    const { id } = req.params;
    mysqlConnection.query('SELECT * FROM ordenes WHERE id = ? ', [id] , (err,rows,fields) => {
        if(err){
            console.log(err);
        }else{
            console.log("Orden obtenida");
            res.send(rows[0]);
        }
    })
});


/**
 * @api {post} /ordenes/insert Postea todos los parametros de una orden
 * @apiGroup Ordenes
 * @apiVersion 0.0.1
 * @apiParamExample {json} Parametros de una orden
 *     {
*       "descripcion": string,
*"esResuelto" : bool,
 *"numeroOrden" : rows.length + 1,
 *"idElemento" : int,
 *"prioridad" : string,
 *"nombreElemento" : string
*     }
 */
router.post('/ordenes/insert', (req, res) => {

                    let post = {descripcion: req.body.descripcion,esResuelto : req.body.esResuelto, numeroOrden : rows.length + 1, idElemento : req.body.idElemento,prioridad : req.body.prioridad,nombreElemento : req.body.nombreElemento};
                    let sql = 'INSERT INTO ordenes SET ?';
                    let query = mysqlConnection.query(sql, post, (err, result) => {
                        if(err){
                            console.log(err);
                        }else{
                            console.log("Bomba nueva");
                            res.send(result);
                        }

            });

});

/**
 * @api {put} /ordenes/actualizacion/:id Actualiza columna esResuelto por id
 * @apiGroup Ordenes
 * @apiVersion 0.0.1
 * @apiParamExample {json} columna esResuelto
 *     {
*       " esResuelto": bool
*     }
 */
router.put('/ordenes/actualizacion/:id', (req, res) => {

    const { id } = req.params;
    let post = { esResuelto : req.body.esResuelto};
    let sql = 'UPDATE ordenes SET ? WHERE id =' + [id] ;
    let query = mysqlConnection.query(sql, post, (err, result) => {
        if(err){
            console.log(err);
        }else{
            console.log("Orden actualizada");
            console.log(req.body.esResuelto);
            res.send(result);
        }

    });
});


/**
 * @api {put} /ordenes/actualizacion/todo/:id Actualiza columnas descripcion y esResuelto por id
 * @apiGroup Ordenes
 * @apiVersion 0.0.1
 * @apiParamExample {json} descripcion y esResuleto
 *     {
*       " descripcion": string,
*"esResuelto" : bool
*     }
 */
router.put('/ordenes/actualizacion/todo/:id', (req, res) => {

    const { id } = req.params;
    let post = { descripcion : req.body.descripcion,esResuelto : req.body.esResuelto};
    let sql = 'UPDATE ordenes SET ? WHERE id =' + [id] ;
    let query = mysqlConnection.query(sql, post, (err, result) => {
        if(err){
            console.log(err);
        }else{
            console.log("Orden actualizada");
            console.log(req.body.esResuelto);
            res.send(result);
        }

    });
});

/**
 * @api {put} /ordenes/control/:id Actualiza las ordenes desde el control de la app android
 * @apiGroup Ordenes
 * @apiVersion 0.0.1
 * @apiParamExample {json} Todos los valores desde el control
 *     {
*       " descripcion": string,
*"esResuelto":bool,
*"prioridad":string,
*"nombreElemento":string,
*     }
 */
router.put('/ordenes/control/:id', (req, res) => {

    const { id } = req.params;
    let post = { descripcion : req.body.descripcion,esResuelto : req.body.esResuelto,prioridad : req.body.prioridad,nombreElemento : req.body.nombreElemento};
    let sql = 'UPDATE ordenes SET ? WHERE id =' + [id] ;
    let query = mysqlConnection.query(sql, post, (err, result) => {
        if(err){
            console.log(err);
        }else{
            console.log("Orden actualizada");
            console.log(req.body.esResuelto);
            res.send(result);
        }

    });
});


module.exports = router;
