const express = require('express');
const router = express.Router();
const mysqlConnection = require('../../database');


/**
 * @api {get} /variador Obtener si el variador esta activo o no
 * @apiGroup Simulador
 * @apiVersion 0.0.1
 */
router.get('/variador',(req,res) => {
    mysqlConnection.query('SELECT * FROM variador   ' , (err,rows,fields) => {
        if(err){
            console.log(err);
        }else{
            console.log("variador obtenidas");
            res.send(rows);
        }
    })
});

/**
 * @api {put} /variador/:id Actualiza valor del variador
 * @apiGroup Simulador
 * @apiVersion 0.0.1
 * @apiParamExample {json} activo o no activo
 *     {
*       " activo": bool
*     }
 */
router.put('/variador/:id', (req, res) => {

    const { id } = req.params;
    let post = { activo : req.body.activo};
    let sql = 'UPDATE variador SET ? WHERE id =' + [id] ;
    let query = mysqlConnection.query(sql, post, (err, result) => {
        if(err){
            console.log(err);
        }else{
            console.log("Orden actualizada");
            console.log(req.body.esResuelto);
            res.send(result);
        }

    });
});





module.exports = router;
